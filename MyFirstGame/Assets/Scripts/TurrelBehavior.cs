﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TurrelBehavior : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    Camera cam;
    [SerializeField]
    Transform Turrel;
    [SerializeField]
    GameObject Bull;
    [SerializeField]
    Transform pointCreate;
    LvlMenager lvlMenager;
    [SerializeField]
    float ShootForce;
   
    

    
    void Start()
    {
       
        if (Turrel == null)
        {
            Turrel = GameObject.FindGameObjectWithTag("Player").transform;
        }
        
        if (cam == null)
        {
            cam = Camera.main;
        }
        lvlMenager = cam.GetComponent<LvlMenager>();
    }
   
    public virtual void OnPointerDown(PointerEventData ped)
    {
        Vector3 point;
        point = cam.ScreenToWorldPoint(new Vector3(ped.position.x, ped.position.y, cam.nearClipPlane + 10));
        Turrel.LookAt(point);
        Shoot();
    }
    IEnumerator DestroyAmmo(GameObject destGM)
    {
        
        yield return new WaitForSeconds(4);
        Destroy(destGM);
        StopCoroutine(DestroyAmmo(destGM));

    }
    void Shoot()
    {
        if(lvlMenager.GetCountbullet() > 0)
        {
            GameObject Createobg = Instantiate(Bull);
            Createobg.transform.position = pointCreate.position;
            Createobg.transform.rotation = pointCreate.rotation;
            Createobg.GetComponent<Rigidbody>().AddForce(Createobg.transform.forward * ShootForce);
            lvlMenager.MinusAmmo();
            StartCoroutine(DestroyAmmo(Createobg));
            
        }
        else
        {
            lvlMenager.GameOver();
        }
    }

}