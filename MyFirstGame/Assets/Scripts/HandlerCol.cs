﻿
using UnityEngine;
using UnityEngine.UI;

public class HandlerCol : MonoBehaviour
{
    [SerializeField]int BlockFall;
    [SerializeField]int BlockAll;
    public LvlMenager lvlMen;
    [SerializeField]
    Slider LvlProggres;

    void Start()
    {
        
        BlockFall = 0;
        BlockAll = lvlMen.SendAllBlock();


    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Bullet")
        {
            BlockFall++;
            Destroy(other.gameObject);
        }
    }
    void Update()
    {
        if(BlockAll != 0)
        {
            LvlProggres.value = 100 / BlockAll * BlockFall;
        }
       
        if (BlockFall == BlockAll && !lvlMen.win)
        {
           
          
            BlockFall = 0;
            lvlMen.NextLvl();
            BlockAll = lvlMen.SendAllBlock();

        }
    }
}
