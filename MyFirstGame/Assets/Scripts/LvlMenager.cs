﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LvlMenager : MonoBehaviour
{
    [SerializeField] GameObject[] LvlPrefs;
    int CountBullet;
    [SerializeField]   int IndexLvl;
    [SerializeField]
    Text AmmoText;
    [SerializeField]
    GameObject loss;
    [SerializeField]
    GameObject Win;
    public Text LevelNext;
   public  Text LvlNow;
    int CountBlocksAll;
    GameObject LVL;
    public bool win;

    public int SendAllBlock()
    {
        return CountBlocksAll;
    }
    void Start()
    {
        if (!PlayerPrefs.HasKey("Progress"))
        {
            PlayerPrefs.SetInt("Progress", 0);
        }
        LVL = null;
        Win.SetActive(false);
        loss.SetActive(false);
        IndexLvl = PlayerPrefs.GetInt("Progress");
    }
   
    void Update()
    {
        
        if(IndexLvl < LvlPrefs.Length)
        {
            LevelNext.text = "" + (IndexLvl + 1);
        }
        else
        {
            LevelNext.text = "win";
        }
       if(IndexLvl <= LvlPrefs.Length)
        {
            LvlNow.text = "" + IndexLvl;
           
        }
        else
        {

            LvlNow.text = "win";
        }

        AmmoText.text = "" + CountBullet;
    }
    public int GetCountbullet()
    {
        return CountBullet;
    }
    public void MinusAmmo()
    {
        CountBullet--;
    }
    public void NextLvl()
    {
      
        if (LVL != null)
        {
            Destroy(LVL);
        }

        PlayerPrefs.SetInt("Progress", IndexLvl);
        IndexLvl++;
        
        FacebookMenager.Instance.LogCompliteLvlEvent(IndexLvl.ToString());
        if (IndexLvl <= LvlPrefs.Length)
        {
            

            LVL = Instantiate(LvlPrefs[IndexLvl-1]);

            CountBullet = IndexLvl * 3;

            CountBlocksAll = LvlPrefs[IndexLvl - 1].GetComponent<LvlProps>().CountBlocks;
        }
        else
        {
            win = true;
            Win.SetActive(true);
        }
      
    }
    public void GameOver()
    {
        loss.SetActive(true);
    }
    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }
   
    public void StartOver()
    {
        PlayerPrefs.DeleteAll();
        print(PlayerPrefs.GetInt("Progress"));
        SceneManager.LoadScene(0);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
