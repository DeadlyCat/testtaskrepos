﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
public class LvlMenager : MonoBehaviour
{
    [SerializeField]
    GameObject[] lvlObj;
    [SerializeField]
    Text TextLvl;
    int LvlIndex;
    [SerializeField]
    GameObject LosePanel;
    [SerializeField]
    GameObject WinPanel;
    int maxlvl;
    [SerializeField]
    Transform StartLvlPos;
    [SerializeField] Transform Player;

    void Start()
    {
        
        if (!PlayerPrefs.HasKey("ProgressLvl"))
        {
            PlayerPrefs.SetInt("ProgressLvl", 0);

        }
        LvlIndex = PlayerPrefs.GetInt("ProgressLvl");
        Player.transform.position = StartLvlPos.position;
        for (int i = 0; i < lvlObj.Length; i++)
        {
            lvlObj[i].SetActive(false);
        }
      
        LosePanel.SetActive(false);
        WinPanel.SetActive(false);
    }
    public void NextLvl()
    {
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Player.transform.position = StartLvlPos.position;
        lvlObj[LvlIndex].SetActive(false);
        LvlIndex++;
        GAMenager.GaMen.CompliteLvl(LvlIndex);
        PlayerPrefs.SetInt("ProgressLvl", LvlIndex);
        if (LvlIndex <lvlObj.Length)
        {

            lvlObj[LvlIndex].SetActive(true);
        }

    }
    public void TryAgain()
    {
        SceneManager.LoadScene(0);
    }
    public void StartOver()
    {
        SceneManager.LoadScene(0);
        PlayerPrefs.DeleteAll();
    }
    public void Lose()
    {
        LosePanel.SetActive(true);
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void Win()
    {
        WinPanel.SetActive(true);
    }
    void Update()
    {
       if(LvlIndex < lvlObj.Length)
        {
            TextLvl.text = "" + (LvlIndex + 1);
            lvlObj[LvlIndex].SetActive(true);
        }
        else
        {
            Win();
            TextLvl.text = "Win";
        }
     
       
    }
}
