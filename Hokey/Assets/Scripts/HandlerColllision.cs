﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandlerColllision : MonoBehaviour
{
   [SerializeField] LvlMenager LvlMen;

   void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag == "Enemy")
        {
            LvlMen.Lose();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Finish")
        {
            LvlMen.NextLvl();
        }
    }
}
