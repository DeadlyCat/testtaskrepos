﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using System;

public class GAMenager : MonoBehaviour
{
    public static GAMenager GaMen;
    void Awake()
    {
        GaMen = this;
        DontDestroyOnLoad(this);
    }
    void Start()
    {
        GameAnalytics.Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public  void CompliteLvl(int Lvl)
    {
        GameAnalytics.NewDesignEvent("CompiteLvl:" +Lvl );
    }
}
