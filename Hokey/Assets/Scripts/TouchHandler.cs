﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TouchHandler : MonoBehaviour ,IPointerDownHandler  , IPointerUpHandler
{
    
    Vector2 PointDown;
    [SerializeField]
    Transform Player;
    void Start()
    {
        
    }
    public virtual void OnPointerDown(PointerEventData ped)
    {
        PointDown = ped.position;
    }
    public virtual void OnPointerUp(PointerEventData ped)
    {
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Vector2 targetDir = ped.position - PointDown;
        Player.rotation = Quaternion.FromToRotation(Vector2.up, targetDir);
        Player.GetComponent<Rigidbody2D>().AddForce(Player.up * 100);
    }


}
